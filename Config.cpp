#include <Config.h>

#include <arpa/inet.h>

#include <cstring>
#include <fstream>
#include <iostream>
#include <set>
#include <sstream>

Config::Config(const std::string &fileName, const std::string &portLabel,
			   const std::string &limitDatagramLabel, const std::string &nodesAddressLabel):
			   _config(InitializeMapConfigFromFile(fileName, portLabel, limitDatagramLabel, nodesAddressLabel)),
			   _port(std::stoul(_config.at(portLabel))),
			   _limitDatagram(std::stoull(_config.at(limitDatagramLabel))),
			   _nodesSockaddr(InitializeNodesSockaddr(nodesAddressLabel))
{
	if(_limitDatagram == 0)
		std::runtime_error("The Datagram Limit cannot be equal to 0!");
}

uint16_t Config::GetPort() const
{
	return _port;
}

size_t Config::GetLimit() const
{
	return _limitDatagram;
}

sockaddr_in Config::GetSockaddrNode(uint64_t numNode) const
{
	if(numNode < 0 || numNode >= _nodesSockaddr.size())
		throw std::out_of_range("Vector out of range");
	return _nodesSockaddr[numNode];
}

size_t Config::GetCountNodes() const
{
	return _nodesSockaddr.size();
}

sockaddr_in Config::IPv4ToSockaddr(const std::string& nodeIPv4) const
{
	sockaddr_in sockaddr{};
	std::istringstream nodeIPv4Stream(nodeIPv4);
	std::string ip;
	std::getline(nodeIPv4Stream, ip, ':');
	std::istringstream ipStream(ip);
	uint16_t num;
	char dot, dot2, dot3;
	ipStream >> num >> dot >> num >> dot2 >> num >> dot3 >> num;
	nodeIPv4Stream >> num;
	if (nodeIPv4Stream && ipStream && nodeIPv4Stream.eof() && ipStream.eof() && dot == '.' && dot2 == '.' && dot3 == '.')
	{
		sockaddr = InitializeSockaddr(AF_INET, num, ip);
	}
	return sockaddr;
}

std::map<std::string, std::string> Config::InitializeMapConfigFromFile(const std::string fileName, const std::string& portLabel, const std::string& limitDatagramLabel, const std::string& nodesAddressLabel) const
{
	std::ifstream fileStream(fileName);
	if (!fileStream.is_open())
		throw std::runtime_error("Unable to open " + fileName + "!");

	std::map<std::string, std::string> config;
	std::string line;
	while (std::getline(fileStream, line))
	{
		if (line.empty())
			continue;
		std::istringstream lineStream(line);
		std::string key;
		if (std::getline(lineStream, key, '='))
		{
			std::string value;
			if (std::getline(lineStream, value))
				config[key] = value;
		}
	}

	if (config.empty())
		throw std::runtime_error("Config is empty or incorrect format!");
	if (config.find(portLabel) == config.end())
		throw std::runtime_error("Config has not Port Label as " + portLabel + "!");
	if (config.find(nodesAddressLabel) == config.end())
		throw std::runtime_error("Config has not Nodes AddressLabel as " + nodesAddressLabel + "!");
	if (config.find(limitDatagramLabel) == config.end())
		throw std::runtime_error("Config has not Limit Datagram Label " + limitDatagramLabel + "!");

	return config;
}

sockaddr_in Config::InitializeSockaddr(const int& domain, const uint16_t& port, const std::string& ip) const
{
	sockaddr_in sockaddrIn{};
	sockaddrIn.sin_family = domain;
	sockaddrIn.sin_port = htons(port);
	inet_pton(domain, ip.c_str(), &(sockaddrIn.sin_addr));
	return sockaddrIn;
}

vectorSockaddr Config::InitializeNodesSockaddr(const std::string& nodesAddressLabel) const
{
	vectorSockaddr vectorSockaddr;
	std::set<sockaddr_in> setSockaddr;
	std::istringstream nodesStream(_config.at(nodesAddressLabel));
	std::string node;
	while (std::getline(nodesStream, node, ' '))
	{
		if (node.find(':') == std::string::npos
		    || node.find(':') == 0
		    || node.find(':') == node.size() - 1)
			throw std::runtime_error(nodesAddressLabel + " has node \"" + node + "\" which not equal IP format! Example right IP format is 127.0.0.1:6666" + "!");
		if (node.find(':') != node.rfind(':'))
			throw std::runtime_error(nodesAddressLabel + " has node \"" + node + "\" which may be in IPv6 format, but it must be IPv4!");
		sockaddr_in sockaddr = IPv4ToSockaddr(node);
		if (sockaddr.sin_family == 0)
			throw std::runtime_error(nodesAddressLabel + " has node \"" + node + "\" which not equal IPv4 format! Example right IPv4 format is 127.0.0.1:6666" + "!");
		setSockaddr.insert(sockaddr);
	}
	for(auto Sockaddr : setSockaddr)
		vectorSockaddr.push_back(Sockaddr);
	return vectorSockaddr;
}

bool operator<(const sockaddr_in &lhs, const sockaddr_in &rhs)
{
	if (lhs.sin_family < rhs.sin_family)
		return lhs.sin_family < rhs.sin_family;
	if (htons(lhs.sin_port) < htons(rhs.sin_port))
		return htons(lhs.sin_port) < htons(rhs.sin_port);
	return inet_ntoa(lhs.sin_addr) < inet_ntoa(rhs.sin_addr);
}