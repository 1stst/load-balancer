#include <ServerBalancer.h>

#include <exception>
#include <getopt.h>
#include <iostream>

int main(int argc, char **argv)
{
	try
	{
		std::string configFileName("config.cfg"),
					portLabel("port"),
					limitDatagramLabel("limit_datagram"),
					nodesAddressLabel("nodes_address");

		if(argc > 1)
		{
			struct option longOptions[] =
					{
							{"help",               no_argument,       0, 'h'},
							{"cfg",                required_argument, 0, 'c'},
							{"config",             required_argument, 0, 'c'},
							{"portLabel",          required_argument, 0, 'p'},
							{"nodesAddressLabel",  required_argument, 0, 'n'},
							{"limitDatagramLabel", required_argument, 0, 'l'},
							{0,                    0,                 0, 0}
					};

			while (true)
			{
				int optionIndex = 0;
				const char short_opts[] = "hc:p:n:l:";
				int c = getopt_long(argc, argv, short_opts, longOptions, &optionIndex);

				if (c == -1)
					break;

				switch (c)
				{
					case 'h':
						std::cout << "Usage:\n\t" << argv[0] << "[options]\n";
						std::cout << "Options:\n";
						std::cout << "\t-h | --help    usage help text\n";
						std::cout << "\t-c | --cfg | --config <filepath> use file from <filepath> as configure file. Default - config.cfg\n";
						std::cout << "\t-p | --portLabel P use P as Label of Port in <filepath>. Default - port\n";
						std::cout << "\t-n | --portLabel N use N as Label of Nodes Address in <filepath>. Default - limit_datagram\n";
						std::cout << "\t-l | --portLabel L use L as Label of Limit Datagram per sec in <filepath>. Default - nodes_address" << std::endl;
						return 0;

					case 'c':
						configFileName = optarg;
						break;

					case 'p':
						portLabel = optarg;
						break;

					case 'n':
						limitDatagramLabel = optarg;
						break;

					case 'l':
						nodesAddressLabel = optarg;
						break;

					default:
						return -1;
				}
			}

			if (optind < argc)
			{
				std::cout << "No equal with option for " << argv[0];
				while (optind < argc)
					std::cout << " " << argv[optind++];
				std::cout << std::endl;
				return 0;
			}
		}
		else
			std::cout << "No options found. Load Balancer will be started with default parameters. Use " << argv[0] << " -h for learn more." << std::endl;

		ServerBalancer::instance(Config{configFileName, portLabel, limitDatagramLabel, nodesAddressLabel}).StartBalancer();
	}
	catch (std::exception& ex)
	{
		std::cerr << "Exception: " << ex.what() << std::endl;
	}
	return 0;
}