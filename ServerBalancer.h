#ifndef LOAD_BALANCER_SERVERBALANCER_H
#define LOAD_BALANCER_SERVERBALANCER_H

#include <Config.h>

#include <chrono>
#include <queue>

#define BUFFER_LENGTH 1024

class ServerBalancer
{
private:
	Config      _config;
	uint64_t    _numberOfNode;
	sockaddr_in _sockaddrServer;
	int         _sockfd;
	char        _buffer[BUFFER_LENGTH];

public:
	static ServerBalancer& instance(const Config& config);
	ServerBalancer(const ServerBalancer&) = delete;
	ServerBalancer& operator=(const ServerBalancer&) = delete;

	[[noreturn]] void StartBalancer();

private:
	ServerBalancer(const Config& config);

	sockaddr_in InitializeSockaddr (const int& domain, const uint16_t& port, const uint32_t& ip) const;
	void RecvDatagram(sockaddr_in& sockaddrClient, socklen_t& lenSockaddrClient);
	void SendDatagram() const;
	void CheckTimeAndLimit(size_t& counterLimit, std::chrono::high_resolution_clock::time_point& t1, std::queue<std::chrono::high_resolution_clock::time_point>& timeQueue);

};

#endif //LOAD_BALANCER_SERVERBALANCER_H