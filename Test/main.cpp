#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <cstring>
#include <thread>
#include <iostream>
#include <mutex>
#include <string>

std::mutex mutex;

void Send(int sockfd, sockaddr_in sockaddrIn, const char *hello,const size_t numberSendDatagram)
{
	for(int i=0; i<numberSendDatagram; i++)
	sendto(sockfd, (const char *) hello, strlen(hello),
	       0, (const struct sockaddr *) &sockaddrIn,
	       sizeof(sockaddrIn));
}

void Recv(int sockfd, const std::string strTemplate, size_t& counterRightDatagramRecv, size_t& counterAllDatagramRecv)
{
	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;

	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));

	sockaddr_in sockaddrIn{};
	socklen_t lenSockaddrClient = sizeof(sockaddrIn);
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_span{};
	do
	{
		char buffer[1024]{0};
		ssize_t n = recvfrom(sockfd, (char *) buffer, sizeof(buffer),
		                     0, (struct sockaddr *) &sockaddrIn,
		                     &lenSockaddrClient);
		if(n!=-1)
		{
			buffer[n] = '\0';
			std::string strRecv = buffer;
			if (strRecv == strTemplate)
				counterRightDatagramRecv++;
			else
			{
				mutex.lock();
				std::cout << "Recv Datagram not equal with Send Datagram: \"" << strRecv << "\" != \"" << strTemplate << "\"" << std::endl;
				mutex.unlock();
			}
			counterAllDatagramRecv++;
		}
		std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();;
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
	} while (time_span.count() < 1);
}

int CreateSocket(int domain, int type, int protocol)
{
	int sockfd = socket(domain, type, protocol);
	if (sockfd < 0)
		throw std::runtime_error("socket creation failed");
	return sockfd;
}

sockaddr_in CreateSockaddr(uint16_t port)
{
	sockaddr_in sockaddrIn{};
	sockaddrIn.sin_family = AF_INET;
	sockaddrIn.sin_port = htons(port);
	sockaddrIn.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	return sockaddrIn;
}

int CreateAndBindSocket(int domain, int type, int protocol, sockaddr_in sockaddrIn)
{
	int sockfd = CreateSocket(domain, type, protocol);
	if ( bind(sockfd, (const struct sockaddr *)&sockaddrIn, sizeof(sockaddrIn)) < 0 )
	{
		throw std::runtime_error("socket bind failed");
	}
	return sockfd;
}

int main()
{
	const size_t portSend = 6666;
	const size_t portRecv1 = 5555;
	const size_t portRecv2 = 5554;
	size_t numberShouldRecvDatagram = 5;
	const size_t numberSendDatagram = 10;
	if(numberShouldRecvDatagram > numberSendDatagram)
		numberShouldRecvDatagram = numberSendDatagram;
	const char *hello = "Hello from client";
	std::string str = hello;
	try
	{
		sockaddr_in sockaddrSend = CreateSockaddr(portSend);
		sockaddr_in sockaddrRecv1 = CreateSockaddr(portRecv1);
		sockaddr_in sockaddrRecv2 = CreateSockaddr(portRecv2);
		int sockfdSend = CreateSocket(AF_INET, SOCK_DGRAM, 0);
		int sockfdRecv1 = CreateAndBindSocket(AF_INET, SOCK_DGRAM, 0, sockaddrRecv1);
		int sockfdRecv2 = CreateAndBindSocket(AF_INET, SOCK_DGRAM, 0, sockaddrRecv2);


		size_t counterAllDatagramRecv1(0), counterAllDatagramRecv2(0), counterRightDatagramRecv1(0), counterRightDatagramRecv2(0);
		std::thread thrSend(Send, sockfdSend, sockaddrSend, hello, numberSendDatagram);
		std::thread thrRecv1(Recv, sockfdRecv1, str, std::ref(counterRightDatagramRecv1), std::ref(counterAllDatagramRecv1));
		std::thread thrRecv2(Recv, sockfdRecv2, str, std::ref(counterRightDatagramRecv2), std::ref(counterAllDatagramRecv2));

		thrSend.join();
		thrRecv1.join();
		thrRecv2.join();
		size_t sumRightDatagramRecv = counterRightDatagramRecv1 + counterRightDatagramRecv2;
		size_t sumAllDatagramRecv = counterAllDatagramRecv1 + counterAllDatagramRecv2;
		std::cout << "Success Recv Datagram: " << sumRightDatagramRecv << " from "
		          << sumAllDatagramRecv << " All Recv Datagram" << std::endl;
		if (numberShouldRecvDatagram == sumRightDatagramRecv)
			std::cout << "TEST SUCCESS! Number of Success Recv Datargam equal Must Recv Datagram" << std::endl;
		else std::cout << "TEST FAIL! Number of Success Recv Datargam not equal Must Recv Datagram" << std::endl;
	}
	catch (const std::exception &ex)
	{
		std::cout << ex.what();
		return 1;
	}
	return 0;
}
