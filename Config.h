#ifndef CLIENT_SERVER_CONFIG_H
#define CLIENT_SERVER_CONFIG_H

#include <netinet/in.h>

#include <map>
#include <string>
#include <vector>

typedef std::vector<sockaddr_in> vectorSockaddr;

class Config
{
private:
	std::map<std::string, std::string>  _config;
	uint16_t                            _port;
	uint64_t                            _limitDatagram;
	vectorSockaddr                      _nodesSockaddr;

public:
	Config(const std::string& fileName, const std::string& portLabel, const std::string& limitDatagramLabel, const std::string& nodesAddressLabel);

	uint16_t GetPort() const;
	size_t GetLimit() const;
	sockaddr_in GetSockaddrNode(uint64_t numNode) const;
	size_t GetCountNodes() const;

private:
	sockaddr_in IPv4ToSockaddr(const std::string& nodeIPv4) const;
	sockaddr_in InitializeSockaddr (const int& domain, const uint16_t& port, const std::string& ip) const;
	std::map<std::string, std::string> InitializeMapConfigFromFile(const std::string fileName, const std::string& portLabel,
																   const std::string& limitDatagramLabel, const std::string& nodesAddressLabel) const;
	vectorSockaddr InitializeNodesSockaddr(const std::string& nodesAddressLabel) const;

};

bool operator < (const sockaddr_in& lhs, const sockaddr_in& rhs);

#endif //CLIENT_SERVER_CONFIG_H