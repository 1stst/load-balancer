#include <ServerBalancer.h>

#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <cstring>
#include <iostream>
#include <thread>
#include <unistd.h>

ServerBalancer::ServerBalancer(const Config& config):
				_config(config),
				_numberOfNode(0),
				_sockaddrServer(InitializeSockaddr(AF_INET, _config.GetPort(), INADDR_ANY)),
				_sockfd (socket(AF_INET, SOCK_DGRAM, 0))
{
	if (_sockfd < 0)
		throw std::runtime_error("socket creation failed");
}

ServerBalancer &ServerBalancer::instance(const Config& config)
{
	static ServerBalancer instance(config);
	return instance;
}

[[noreturn]] void ServerBalancer::StartBalancer()
{
	if ( bind(_sockfd, (const struct sockaddr *)&_sockaddrServer, sizeof(_sockaddrServer)) < 0 )
	{
		throw std::runtime_error("socket bind failed");
	}

	std::chrono::high_resolution_clock::time_point t1{};
	size_t counterLimit = _config.GetLimit();
	const size_t cashedCountNodes = _config.GetCountNodes();
	sockaddr_in sockaddrClient;
	memset(&sockaddrClient, 0, sizeof(sockaddrClient));
	socklen_t lenSockaddrClient = sizeof(sockaddrClient);
	std::queue<std::chrono::high_resolution_clock::time_point> timeQueue;

	while(true)
	{
		memset( _buffer, '\0', BUFFER_LENGTH);
		RecvDatagram(sockaddrClient, lenSockaddrClient);
		if(_buffer[0] == '\0')
			continue;////Not sure what to do with empty datagrams

		SendDatagram();
		CheckTimeAndLimit(counterLimit, t1, timeQueue);
		_numberOfNode = ++_numberOfNode % cashedCountNodes;
	}
}

sockaddr_in ServerBalancer::InitializeSockaddr(const int& domain, const uint16_t& port, const uint32_t& ip) const
{
	sockaddr_in sockaddrIn{};
	sockaddrIn.sin_family = domain;
	sockaddrIn.sin_port = htons(port);
	sockaddrIn.sin_addr.s_addr = htonl(ip);
	return sockaddrIn;
}

void ServerBalancer::RecvDatagram(sockaddr_in& sockaddrClient, socklen_t& lenSockaddrClient)
{
	ssize_t n = recvfrom(_sockfd, (char*) _buffer, sizeof (_buffer),
	                     0, (struct sockaddr *) &sockaddrClient,
	                     &lenSockaddrClient);
	if(n <= 0)
		return;
	_buffer[n] = '\0';
	std::cout << "\nReceive datagram from: " << inet_ntoa(sockaddrClient.sin_addr) << ":" << htons(sockaddrClient.sin_port) << std::endl;
}

void ServerBalancer::SendDatagram() const
{
	sockaddr_in servaddrSend = _config.GetSockaddrNode(_numberOfNode);
	sendto(_sockfd, (const char *) _buffer, strlen(_buffer),
	       0, (const struct sockaddr *) &servaddrSend,
	       sizeof(servaddrSend));
	std::cout << "Datagram send to: " << inet_ntoa(servaddrSend.sin_addr) << ":" << htons(servaddrSend.sin_port) << std::endl;
}

void ServerBalancer::CheckTimeAndLimit(size_t &counterLimit, std::chrono::high_resolution_clock::time_point& t1, std::queue<std::chrono::high_resolution_clock::time_point>& timeQueue)
{
	counterLimit--;

	if (t1.time_since_epoch().count() == 0)
	{
		t1 = std::chrono::high_resolution_clock::now();
		if (counterLimit != 0)
			return;
	}

	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
	timeQueue.push(t2);
	if (_config.GetLimit() != 1)
	{
		while (time_span.count() > 1)
		{
			counterLimit++;
			t1 = timeQueue.front();
			timeQueue.pop();
			time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
		}
		if (counterLimit != 0)
			return;
	}

	std::cout << "\nLimit datagram per second exceeded! Datagrams will be ignored!" << std::endl;
	close(_sockfd);
	uint64_t timeWait = (uint64_t) ((1 - time_span.count()) * 1000000000);
	std::this_thread::sleep_for(std::chrono::nanoseconds(timeWait));

	std::cout << "Reset the datagram limit per second! Datagrams will be accepted!" << std::endl;
	_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (bind(_sockfd, (const struct sockaddr *) &_sockaddrServer, sizeof(_sockaddrServer)) < 0)
	{
		throw std::runtime_error("socket bind failed");
	}

	counterLimit++;
	t1 = timeQueue.front();
	timeQueue.pop();
}